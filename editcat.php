<link rel="stylesheet" href="./ressources/style5.css">

<?php
include('./connect.php');

if (isset($_POST['name'])){
    $update = $pdo->prepare('UPDATE categories SET categories.name = :name WHERE categories.id = :currentid');
    $update->bindParam(':name', $_POST['name']);
    $update->bindParam(':currentid', $_GET['id']);
    $update->execute();
}

$affichage = $pdo->prepare('SELECT c.name as "category name", c.id as "category id" FROM categories as c WHERE c.id = :currentid');
$affichage->bindParam(':currentid', $_GET['id']);
$affichage->execute();
$result = $affichage->fetch();

?>


<div class="formulaire">

    <div class="form_header">
        <a href="./index.php"><img id="logo" src="./ressources/logo.png"></a>
    </div>

    <form action="./editcat.php?id=<?php echo $_GET['id'] ?>" method="post">
        <p><a href="./catadd.php">Back</a></p>
        <p>EDIT NAME</p>
        <p><input type="text" name="name" placeholder="<?php echo $result['category name'] ?>"></p>
        <p><input type="submit"></p>
    </form>    
</div>