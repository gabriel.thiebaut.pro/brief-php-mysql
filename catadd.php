<link rel="stylesheet" href="./ressources/style3.css">
<?php
include('./connect.php');

if (isset($_GET["id"])) {
    $stmt = $pdo->prepare('DELETE FROM categories WHERE categories.id = :idama');
    $stmt->bindParam(':idama', $_GET['id']);
    $stmt->execute();
    $stmt->closeCursor();    
};

$reponse2 = $pdo->prepare('SELECT c.name as "name cat", c.id as "id cat" FROM categories as c');
$reponse2->execute();
$result2 = $reponse2->fetchAll();

if (isset ($_POST['nom'])){
    $stmt = $pdo->prepare('INSERT INTO categories (name) VALUES(:name)');
    $stmt->bindParam(':name', $_POST['nom'], PDO::PARAM_STR);
    $stmt->execute();
    $stmt->closeCursor();
};

?>

<div class="header">
    <div class="hd_logo">
        <a href="./index.php"><img id="logo" src="./ressources/logo.png"></a>
    </div>
    <div class="hd_header">
        <h1>BOOKMARK CATEGORIES</h1>
    </div>
    <div class="hd_nav">
        <nav class="header_nav">
            <span><a href="./index.php">Bookmark List</a></span>
        </nav>
    </div>
</div>

<div class="content">
    <div class="formulaire">
        <form action="catadd.php" method="post">
                <p>Category Name</p>
                <p><input type="text" name="nom"></p>
                <p><input type="submit" name="submit" value="Create"></p>
        </form>    
    </div>    
</div>

<main>
    <table class="index_table">
        <tr class="header_table">
            <td>NAME</td>
            <td>ACTIONS</td>
        </tr>
        <?php foreach($result2 as $r2) : ?>
        <tr>
            <td class="nametag"><?php echo $r2['name cat']?></td>
            <td class="actions"><a href="./editcat.php?id=<?php echo $r2['id cat']?>"><button>EDIT</button></a> <a href="./catadd.php?id=<?php echo $r2['id cat']?>"><button>DELETE</button></a></td>
        </tr>
        <?php endforeach; ?>
        <tr class="footer_table">
                    <td>NAME</td>
                    <td>ACTIONS</td>
                </tr>
    </table>    
</main>