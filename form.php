<link rel="stylesheet" href="./ressources/style2.css">
<?php
include('./connect.php');

$reponse2 = $pdo->prepare('SELECT c.name as "name cat", c.id as "id cat" FROM categories as c');
$reponse2->execute();
$result2 = $reponse2->fetchAll();

if (isset ($_POST['nom']) && $_POST['url']){
    $stmt = $pdo->prepare('INSERT INTO links (name, url) VALUES(:name, :url)');
    $stmt->bindParam(':name', $_POST['nom'], PDO::PARAM_STR);
    $stmt->bindParam(':url', $_POST['url'], PDO::PARAM_STR);
    $stmt->execute();
    $last_id = $pdo->lastInsertId();

    if(isset($_POST['categorie'])){
        foreach($_POST['categorie'] as $onecategory):
        $stmt = $pdo->prepare('INSERT INTO search ( id_links, id_cat ) VALUES( :idl, :idcc )');
        $stmt->bindParam(':idl', $last_id);
        $stmt->bindParam(':idcc', $onecategory);
        $stmt->execute();
        endforeach;
        $stmt->closeCursor();        
    }
    else{
        $stmt = $pdo->prepare('INSERT INTO search ( id_links, id_cat ) VALUES( :idl, 0 )');
        $stmt->bindParam(':idl', $last_id);
        $stmt->execute();
        $stmt->closeCursor(); 
    }
};
?>

<body>
    <div class="formulaire">

        <div class="form_header">
            <a href="./index.php"><img id="logo" src="./ressources/logo.png"></a>
        </div>

        <div class="form_content">
            <form action="form.php" method="post">
                <p>BOOKMARK NAME</p>
                <input type="text" name="nom">
                <P>BOOKMARK URL</p>
                <input type="text" name="url">
                <div class="checkboxes">
                    <?php foreach($result2 as $resultat):?>
                        <p><input type="checkbox" value="<?php echo $resultat["id cat"]?>" name="categorie[]"><?php echo $resultat["name cat"]?></p>
                    <?php endforeach; ?>
                </div>
                <div class="edit">
                    <a href="./catadd.php">Edit Categories</a>    
                </div>
                <input type="submit" name="submit" value="ADD BOOKMARK">
            </form>        
        </div>     

    </div>
</body>