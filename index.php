<link rel="stylesheet" href="./ressources/style.css">

<?php
include('./connect.php');

if (isset($_GET["id"])) {
    $stmt = $pdo->prepare('DELETE FROM links WHERE links.id = :idama');
    $stmt->bindParam(':idama', $_GET['id']);
    $stmt->execute();
    $stmt->closeCursor();
};

$reponse2 = $pdo->prepare('SELECT c.name as "name cat", c.id as "id cat" FROM categories as c');
$reponse2->execute();
$result2 = $reponse2->fetchAll();

if (isset($_POST['filter'])) {
    $affichage = $pdo->prepare('SELECT l.name as "name link", l.url as "lien link", l.id as "id link", GROUP_CONCAT(c.name SEPARATOR " / ") as "category name" FROM links as l 
    left join search as lc ON l.id = lc.id_links 
    left join categories as c ON c.id = lc.id_cat
    GROUP BY l.id
    WHERE c.id = :idfilter;');
    $affichage->bindParam(':idfilter', $_POST['filter']);
    $affichage->execute();
    $result = $affichage->fetchAll(PDO::FETCH_ASSOC);
} else {
    $affichage = $pdo->prepare('SELECT l.name as "name link", l.url as "lien link", l.id as "id link", GROUP_CONCAT(c.name SEPARATOR " / ") as "category name" FROM links as l
    left join search as lc ON l.id = lc.id_links 
    left join categories as c ON c.id = lc.id_cat
    GROUP BY l.id;');
    $affichage->execute();
    $result = $affichage->fetchAll(PDO::FETCH_ASSOC);
}

?>

<div class="header">
    <div class="hd_logo">
        <a href="./index.php"><img id="logo" src="./ressources/logo.png"></a>
    </div>
    <div class="hd_header">
        <h1>MY BOOKMARKS</h1>
    </div>
    <div class="hd_nav">
        <nav class="header_nav">
            <span><a href="./form.php">Add Bookmarks</a></span>
            <span><a href="./catadd.php">Add / Edit Categories</a></span>
        </nav>
    </div>
</div>

<main>
    <div class="div_table">
        <table class="index_table">
            <tr class="header_table">
                <td>NAME</td>
                <td>LINK</td>
                <td>CATEGORY</td>
                <td>ACTIONS</td>
            </tr>
            <?php foreach ($result as $datas) : ?>
            <tr>
                <td class="nametag"><?php echo $datas['name link'] ?></td>
                <td class="nametag"><a href="<?php echo $datas['lien link'] ?>">Open <?php echo $datas['name link'] ?></a></td>
                <td class="nametag">
                <?php
                if(empty ($datas['category name'])){
                    $datas['category name'] = 'No Category';
                    echo $datas['category name'];
                }
                else {
                    echo $datas['category name'];
                };
                ?>
                </td>
                <td class="actions">
                    <a href="./modif.php?id=<?php echo $datas['id link'] ?>"><button>EDIT</button></a>
                    <a href="./index.php?id=<?php echo $datas['id link'] ?>"><button>DELETE</button></a>
                </td>
            </tr>
            <?php endforeach; ?>
            <tr class="footer_table">
                <td>NAME</td>
                <td>LINK</td>
                <td>CATEGORY</td>
                <td>ACTIONS</td>
            </tr>
        </table>
    </div>
</main>