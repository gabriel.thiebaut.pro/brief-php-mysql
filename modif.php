<link rel="stylesheet" href="./ressources/style4.css">

<?php 
include('./connect.php');

$checked = '';

/// Modif des données si post name est rempli

    $verify = $pdo->prepare('SELECT *
    FROM `search`
    WHERE EXISTS (
    SELECT id_links
    FROM `search`
    WHERE id_links = :idcurrent)');
    $verify->bindParam(':idcurrent', $_GET['id']);
    $verify->execute();
    $resultot = $verify->fetchAll();

    if(empty($resultot)){
        $insertion = $pdo->prepare('INSERT INTO search (id_cat, id_links) VALUES(:idcategory, :idcurrent)');
        $insertion->bindParam('idcategory', $_POST['categorie']);
        $insertion->bindParam('idcurrent', $_POST['id']);
        $insertion->execute();
        $insertion->closeCursor();
    }
    else {
        
        if (isset($_POST["name"]) && $_POST["url"]) {
            $stmt = $pdo->prepare('UPDATE links SET links.name = :name , links.url = :url WHERE links.id = :idama');
            $stmt->bindParam(':idama', $_POST['id']);
            $stmt->bindParam(':name', $_POST['name']);
            $stmt->bindParam(':url', $_POST['url']);
            $stmt->execute();
            $stmt->closeCursor();
        };

        if(isset($_POST['categorie']) && !empty($_POST['categorie'])){
            $stmt2 = $pdo->prepare('DELETE FROM search WHERE id_links = :idama');
            $stmt2->bindParam(':idama', $_POST['id']);
            $stmt2->execute();

            foreach($_POST['categorie'] as $onecat):
                $stmt5 = $pdo->prepare('INSERT INTO search (id_cat, id_links) VALUES(:idcats, :currentid)');
                $stmt5->bindParam(':idcats', $onecat);
                $stmt5->bindParam(':currentid', $_GET['id']);
                $stmt5->execute();
                $stmt5->closeCursor();
            endforeach;

            $stmt2->closeCursor();
        };      
    }
/// On va chercher les données dans la db

$affichage = $pdo->prepare('SELECT c.id as "category id s" FROM search AS s INNER JOIN categories AS c ON c.id = s.id_cat WHERE id_links = :idurl');
$affichage->bindParam(':idurl', $_GET['id']);
$affichage->execute();
$result3 = $affichage->fetchAll();

$reponse = $pdo->prepare('SELECT l.name as "name link", l.url as "lien link", l.id as "id link" FROM links as l where l.id = :idurl');
$reponse->bindParam(':idurl', $_GET['id']);
$reponse->execute();
$result = $reponse->fetch();

$reponse2 = $pdo->prepare('SELECT c.name as "name cat", c.id as "id cat" FROM categories as c');
$reponse2->execute();
$result2 = $reponse2->fetchAll();

?>

<div class="formulaire">
    
    <div class="form_header">
        <a href="./index.php"><img id="logo" src="./ressources/logo.png"></a>
    </div>
    <form action="modif.php?id=<?php echo $_GET['id']?>" method="post">
        <input type="hidden" name="id" value=<?php echo $result["id link"]?>>
        <p>EDIT NAME</p>
        <input type="text" value="<?php echo $result["name link"]?>" name="name">
        <p>EDIT URL</p>
        <input type="text" value="<?php echo $result["lien link"]?>" name="url">
        <p>
        <div class="checkboxes">
            <?php foreach($result2 as $resultat):?>
                    <?php 
                    $checked='';
                    foreach($result3 as $r3):
                        if($resultat['id cat'] == $r3['category id s']){
                            $checked='checked="checked"';
                        }
                    endforeach;
                    ?>
                <p><input type="checkbox" value="<?php echo $resultat["id cat"]?>" <?php echo $checked ?> name="categorie[]"><?php echo $resultat["name cat"]?></p>
            <?php endforeach; ?>   
        </div>             
        </p>
        <div class="edit">
            <a href="./catadd.php">Edit Categories</a>    
        </div>
        <p><input type="submit" value="EDIT BOOKMARK"></p>
    </form>
</div>

<?php
$reponse->closeCursor();
?>